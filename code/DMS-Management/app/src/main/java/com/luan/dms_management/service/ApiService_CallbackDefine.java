package com.luan.dms_management.service;

import com.luan.dms_management.models.Channel;
import com.luan.dms_management.models.Customer;
import com.luan.dms_management.models.CustomerGroup;
import com.luan.dms_management.models.DocStatus;
import com.luan.dms_management.models.InventoryProduct;
import com.luan.dms_management.models.ItemType;
import com.luan.dms_management.models.LeaveLog;
import com.luan.dms_management.models.LeaveReason;
import com.luan.dms_management.models.LeaveType;
import com.luan.dms_management.models.NormalProduct;
import com.luan.dms_management.models.NoteType;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.Orders;
import com.luan.dms_management.models.Product;
import com.luan.dms_management.models.PromotionProduct;
import com.luan.dms_management.models.Route;
import com.luan.dms_management.models.Staff;
import com.luan.dms_management.models.StaffLocation;
import com.luan.dms_management.models.StaffMonitor;
import com.luan.dms_management.models.Statistic;
import com.luan.dms_management.models.StatusOrder;
import com.luan.dms_management.models.TimeSheet;
import com.luan.dms_management.models.TypeOrder;
import com.luan.dms_management.models.TypeVisit;
import com.luan.dms_management.models.WareHouse;

import java.util.ArrayList;
import java.util.List;

public class ApiService_CallbackDefine extends ApiService_Base {
    public interface CommonCallback {
        void onSuccess();

        void onFail(String error);
    }
    public interface LoginCallBack {
        void onSuccess(String datetime, String salemanid, String noSecRefresh);

        void onFail(String error);
    }
    public interface GetListStaff {
        void onSuccess(List<Staff> data);

        void onFail(String error);
    }
    public interface GetListProduct {
        void onSuccess(List<Product> data);

        void onFail(String error);
    }
    public interface GetListNormalProduct {
        void onSuccess(List<NormalProduct> data);

        void onFail(String error);
    }
    public interface GetListCustomer {
        void onSuccess(List<Customer> data);

        void onFail(String error);
    }
    public interface GetListCustomerTravel {
        void onSuccess(List<Customer> data);

        void onFail(String error);
    }
    public interface GetListStaffMonitor {
        void onSuccess(List<StaffMonitor> data);

        void onFail(String error);
    }
    public interface GetListStaffLocation {
        void onSuccess(List<StaffLocation> data);

        void onFail(String error);
    }
    public interface GetListRoute {
        void onSuccess(List<Route> data);

        void onFail(String error);
    }
    public interface GetListChannel {
        void onSuccess(List<Channel> data);

        void onFail(String error);
    }
    public interface GetListCusGrp {
        void onSuccess(List<CustomerGroup> data);

        void onFail(String error);
    }
    public interface GetListTypeVisit {
        void onSuccess(List<TypeVisit> data);

        void onFail(String error);
    }
    public interface GetListTypeOrder {
        void onSuccess(List<TypeOrder> data);

        void onFail(String error);
    }
    public interface GetListStatusOrder {
        void onSuccess(List<StatusOrder> data);

        void onFail(String error);
    }
    public interface GetListWareHouse {
        void onSuccess(List<WareHouse> data);

        void onFail(String error);
    }
    public interface GetListOrder {
        void onSuccess(List<Orders> data);

        void onFail(String error);
    }
    public interface GetListOrderbyDoc {
        void onSuccess(ArrayList<OrderbyDoc> data);

        void onFail(String error);
    }
    public interface GetListNoteType {
        void onSuccess(List<NoteType> data);

        void onFail(String error);
    }
    public interface GetListCustGroup {
        void onSuccess(List<NoteType> data);

        void onFail(String error);
    }
    public interface CheckinCallback {
        void onSuccess(String checkinID);

        void onFail(String error);
    }
    public interface GetNoteCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface AddCustCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface GetPromotionProductsCallback {
        void onSuccess(List<PromotionProduct> data);

        void onFail(String error);
    }
    public interface GetPriceCallback {
        void onSuccess(String price, String vat);

        void onFail(String error);
    }
    public interface GetStatisticsCallback {
        void onSuccess(List<Statistic> data);

        void onFail(String error);
    }
    public interface GetInventoryProductCallback {
        void onSuccess(InventoryProduct data);

        void onFail(String error);
    }
    public interface GetLeaveReasonCallback {
        void onSuccess(List<LeaveReason> data);

        void onFail(String error);
    }
    public interface GetLeaveTypeCallback {
        void onSuccess(List<LeaveType> data);

        void onFail(String error);
    }
    public interface AddLeaveCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface GetLeaveLogCallback {
        void onSuccess(List<LeaveLog> data);

        void onFail(String error);
    }
    public interface ApproveLeaveCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface AddTimeSheetCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface LogTimeSheetCallback {
        void onSuccess(List<TimeSheet> data);

        void onFail(String error);
    }
    public interface UpdateTimeSheetCallback {
        void onSuccess(String data);

        void onFail(String error);
    }
    public interface DocStatusCallback {
        void onSuccess(List<DocStatus> data);
        void onFail(String error);
    }
    public interface ItemTypeCallback {
        void onSuccess(List<ItemType> data);
        void onFail(String error);
    }
    public interface CreateOrderCallback{
        void onSuccess(String data);
        void onFail(String error);
    }
}
