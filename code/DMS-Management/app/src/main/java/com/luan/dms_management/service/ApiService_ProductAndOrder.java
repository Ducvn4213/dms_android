package com.luan.dms_management.service;

import android.text.TextUtils;

import com.luan.dms_management.models.DocStatus;
import com.luan.dms_management.models.InventoryProduct;
import com.luan.dms_management.models.ItemType;
import com.luan.dms_management.models.PromotionProduct;
import com.luan.dms_management.models.Statistic;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class ApiService_ProductAndOrder extends ApiService_Customer {

    public static class OrderToCreate {
        public String cardCode;
        public String orderType;
        public String poDate;
        public String deliveryDate;
        public String salesEmp;
        public String docTotal;
        public String vatTotal;
        public String GTotal;
        public String remarks;

        public OrderToCreate() {}
    }

    public static class ProductToAdd {
        public String docEntry;
        public String itemCode;
        public String itemName;
        public String quantity;
        public String uoM;
        public String price;
        public String discount;
        public String priceAfterDiscount;
        public String lineTotal;
        public String vatPercent;
        public String vatAmt;
        public String gTotal;
        public String itemType;
        public String whsCode;

        public ProductToAdd() {}
    }

    public void GetPromotionProducts(String custCode, String userCode, String data, final GetPromotionProductsCallback callback) {
        String link = String.format(getURL() + Config.GETPROMOTIONPRODUCTS);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", custCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("CurrDate", data);
        params.add(param);
        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<PromotionProduct> returnValue = new ArrayList<>();
                try {
                    boolean readyToGetCodeBars = false;
                    boolean readyToGetItemName = false;
                    boolean readyToGetItemCode = false;

                    String currentCodeBars = null;
                    String currentItemName = null;
                    String currentItemCode = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("CodeBars")) {
                                readyToGetCodeBars = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ItemName")) {
                                readyToGetItemName = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ItemCode")) {
                                readyToGetItemCode = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCodeBars) {
                                currentCodeBars = data;
                                readyToGetCodeBars = false;
                                if (!TextUtils.isEmpty(currentCodeBars) && !TextUtils.isEmpty(currentItemName) &&
                                        !TextUtils.isEmpty(currentItemCode)) {

                                    PromotionProduct promotionProduct = new PromotionProduct();
                                    promotionProduct.setCodeBars(currentCodeBars);
                                    promotionProduct.setItemName(currentItemName);
                                    promotionProduct.setItemCode(currentItemCode);

                                    returnValue.add(promotionProduct);

                                    currentCodeBars = null;
                                    currentItemName = null;
                                    currentItemCode = null;
                                }
                            }

                            if (readyToGetItemName) {
                                currentItemName = data;
                                readyToGetItemName = false;
                                if (!TextUtils.isEmpty(currentCodeBars) && !TextUtils.isEmpty(currentItemName) &&
                                        !TextUtils.isEmpty(currentItemCode)) {

                                    PromotionProduct promotionProduct = new PromotionProduct();
                                    promotionProduct.setCodeBars(currentCodeBars);
                                    promotionProduct.setItemName(currentItemName);
                                    promotionProduct.setItemCode(currentItemCode);

                                    returnValue.add(promotionProduct);

                                    currentCodeBars = null;
                                    currentItemName = null;
                                    currentItemCode = null;
                                }
                            }

                            if (readyToGetItemCode) {
                                currentItemCode = data;
                                readyToGetItemCode = false;
                                if (!TextUtils.isEmpty(currentCodeBars) && !TextUtils.isEmpty(currentItemName) &&
                                        !TextUtils.isEmpty(currentItemCode)) {

                                    PromotionProduct promotionProduct = new PromotionProduct();
                                    promotionProduct.setCodeBars(currentCodeBars);
                                    promotionProduct.setItemName(currentItemName);
                                    promotionProduct.setItemCode(currentItemCode);

                                    returnValue.add(promotionProduct);

                                    currentCodeBars = null;
                                    currentItemName = null;
                                    currentItemCode = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess(returnValue);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void GetPriceOfProduct(String custCode, String itemCode, String uom, String date, final GetPriceCallback callback) {
        String link = String.format(getURL() + Config.GETPRICE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", custCode);
        params.add(param);
        param = new Param("ItemCode", itemCode);
        params.add(param);
        param = new Param("UoM", uom);
        params.add(param);
        param = new Param("CurrDate", date);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;
                    boolean readyToGetVAT = false;

                    String currentResult = null;
                    String currentVAT = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ItemPrice")) {
                                readyToGetResult = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("VAT")) {
                                readyToGetVAT = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                readyToGetResult = false;

                                String data = xpp.getText();
                                currentResult = data;

                                if (currentVAT != null) {
                                    callback.onSuccess(currentResult, currentVAT);
                                    return;
                                }
                            }

                            if (readyToGetVAT) {
                                readyToGetVAT = false;

                                String data = xpp.getText();
                                currentVAT = data;

                                if (currentResult != null) {
                                    callback.onSuccess(currentResult, currentVAT);
                                    return;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void addOrder(String cardCode, String userCode, String orderDate, String deliveryDate, String cardName, String itemCode, String itemName, String itemType, String quantity, String uom, String price, String vat, final CommonCallback callback) {
        String link = String.format(getURL() + Config.ADDORDER);

        List<Param> params = new ArrayList<>();
        Param param = new Param("Cardcode", cardCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("OrderDate", orderDate);
        params.add(param);
        param = new Param("DeliveryDate", deliveryDate);
        params.add(param);
        param = new Param("CardName", cardName);
        params.add(param);
        param = new Param("ItemCode", itemCode);
        params.add(param);
        param = new Param("ItemName", itemName);
        params.add(param);
        param = new Param("ItemType", itemType);
        params.add(param);
        param = new Param("Quantity", quantity);
        params.add(param);
        param = new Param("Uom", uom);
        params.add(param);
        param = new Param("Price", price);
        params.add(param);
        param = new Param("VAT", vat);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                callback.onSuccess();
//                                String data = xpp.getText();
//                                if (data.trim().equalsIgnoreCase("1")) {
//                                    callback.onSuccess();
//                                }
//                                else {
//                                    callback.onFail("");
//                                }
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getStatistics(String userCode, String custCode, final GetStatisticsCallback callback) {
        String link = String.format(getURL() + Config.STATISTICS);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", custCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<Statistic> returnValues = new ArrayList<>();
                try {
                    boolean readyToGetCustCode = false;
                    boolean readyToGetCheckInTime = false;
                    boolean readyToGetCheckOutTime = false;
                    boolean readyToGetNoOrder = false;
                    boolean readyToGetNoPic = false;

                    String currentCustCode = null;
                    String currentCheckInTime = null;
                    String currentCheckOutTime = null;
                    String currentNoOrder = null;
                    String currentNoPic = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("CustCode")) {
                                readyToGetCustCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("CheckInTime")) {
                                readyToGetCheckInTime = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("CheckOutTime")) {
                                readyToGetCheckOutTime = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("NoCustOrder")) {
                                readyToGetNoOrder = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("NoPicTake")) {
                                readyToGetNoPic = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCustCode) {
                                currentCustCode = data;
                                readyToGetCustCode = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }

                            if (readyToGetCheckInTime) {
                                currentCheckInTime = data;
                                readyToGetCheckInTime = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }

                            if (readyToGetCheckInTime) {
                                currentCheckOutTime = data;
                                readyToGetCheckInTime = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }
                            if (readyToGetCheckOutTime) {
                                currentCheckOutTime = data;
                                readyToGetCheckOutTime = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }

                            if (readyToGetNoOrder) {
                                currentNoOrder = data;
                                readyToGetNoOrder = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }

                            if (readyToGetNoPic) {
                                currentNoPic = data;
                                readyToGetNoPic = false;
                                if (!TextUtils.isEmpty(currentCustCode) && !TextUtils.isEmpty(currentCheckInTime) &&
                                        !TextUtils.isEmpty(currentNoOrder) && !TextUtils.isEmpty(currentNoPic)) {
                                    Statistic statistic = new Statistic();
                                    statistic.setCustCode(currentCustCode);
                                    statistic.setCheckInTime(currentCheckInTime);
                                    statistic.setCheckOutTime(currentCheckOutTime);
                                    statistic.setNoCustOrder(currentNoOrder);
                                    statistic.setNoPicTake(currentNoPic);

                                    returnValues.add(statistic);

                                    currentCustCode = null;
                                    currentCheckInTime = null;
                                    currentCheckOutTime = null;
                                    currentNoOrder = null;
                                    currentNoPic = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess(returnValues);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getInventoryProduct(String barCode, final GetInventoryProductCallback callback) {
        String link = String.format(getURL() + Config.INVENTORYPRODUCT);

        List<Param> params = new ArrayList<>();
        Param param = new Param("BarCode", barCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetBarcode = false;
                    boolean readyToGetItemCode = false;
                    boolean readyToGetItemName = false;
                    boolean readyToGetUoM = false;
                    boolean readyToGetInvUom = false;
                    boolean readyToGetExpDate = false;

                    String currentBarcode = null;
                    String currentItemCode = null;
                    String currentItemName = null;
                    String currentUoM = null;
                    String currentInvUom = null;
                    String currentExpDate = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("Barcode")) {
                                readyToGetBarcode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ItemCode")) {
                                readyToGetItemCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ItemName")) {
                                readyToGetItemName = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("UoM")) {
                                readyToGetUoM = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("InvUom")) {
                                readyToGetInvUom = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ExpDate")) {
                                readyToGetExpDate = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetBarcode == true) {
                                currentBarcode = data;
                                readyToGetBarcode = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }

                            if (readyToGetItemCode == true) {
                                currentItemCode = data;
                                readyToGetItemCode = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }

                            if (readyToGetItemName == true) {
                                currentItemName = data;
                                readyToGetItemName = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }
                            if (readyToGetUoM == true) {
                                currentUoM = data;
                                readyToGetUoM = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }

                            if (readyToGetInvUom == true) {
                                currentInvUom = data;
                                readyToGetInvUom = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }

                            if (readyToGetExpDate == true) {
                                currentExpDate = data;
                                readyToGetExpDate = false;
                                if (!TextUtils.isEmpty(currentBarcode) && !TextUtils.isEmpty(currentItemCode) && !TextUtils.isEmpty(currentItemName) && !TextUtils.isEmpty(currentInvUom) && !TextUtils.isEmpty(currentUoM) && !TextUtils.isEmpty(currentExpDate)) {
                                    InventoryProduct inventoryProduct = new InventoryProduct();
                                    inventoryProduct.setBarcode(currentBarcode);
                                    inventoryProduct.setItemCode(currentItemCode);
                                    inventoryProduct.setItemName(currentItemName);
                                    inventoryProduct.setUom(currentUoM);
                                    inventoryProduct.setInvUom(currentInvUom);
                                    inventoryProduct.setExpDate(currentExpDate);

                                    callback.onSuccess(inventoryProduct);
                                    return;

//                                    currentBarcode = null;
//                                    currentItemCode = null;
//                                    currentItemName = null;
//                                    currentUoM = null;
//                                    currentInvUom = null;
//                                    currentExpDate = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }

                callback.onFail("");
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void cancelOrder(String userCode, String docEntry, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CANCELORDER);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("DocEntry", docEntry);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetAlert = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("Alert")) {
                                readyToGetAlert = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetAlert) {
                                if (data.equalsIgnoreCase("Unsuccessfully")) {
                                    callback.onFail(data);
                                    return;
                                }

                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }

                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDocStatus(String userCode, final DocStatusCallback callback) {
        String link = String.format(getURL() + Config.GETDOCSTATUS);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<DocStatus> docStatusList = new ArrayList<>();
                try {
                    boolean readyToGetStatus = false;
                    boolean readyToGetName = false;


                    String currentStatus = null;
                    String currentName = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("DocStatus")) {
                                readyToGetStatus = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("DocStatusName")) {
                                readyToGetName = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetStatus) {
                                currentStatus = data;
                                readyToGetStatus = false;
                                if (!TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentName)) {
                                    DocStatus docStatus = new DocStatus();
                                    docStatus.setDocStatus(currentStatus);
                                    docStatus.setDocStatusName(currentName);

                                    docStatusList.add(docStatus);

                                    currentStatus = null;
                                    currentName = null;
                                }
                            }

                            if (readyToGetName) {
                                currentName = data;
                                readyToGetName = false;
                                if (!TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentName)) {
                                    DocStatus docStatus = new DocStatus();
                                    docStatus.setDocStatus(currentStatus);
                                    docStatus.setDocStatusName(currentName);

                                    docStatusList.add(docStatus);

                                    currentStatus = null;
                                    currentName = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess(docStatusList);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateDelivery(String docEntry, String date, String status, String note, final CommonCallback callback) {
        String link = String.format(getURL() + Config.UPDATEDELIVERY);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", getUserCode());
        params.add(param);

        param = new Param("DocEntry", docEntry);
        params.add(param);

        param = new Param("DeliveryDate_Act", date);
        params.add(param);

        param = new Param("DocStatus", status);
        params.add(param);

        param = new Param("NotesDelivery", note);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("Alert")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                if (data.contains("UnSuccessfully")) {
                                    callback.onFail(data);
                                    return;
                }

                callback.onSuccess();
        return;
    }
}
                        eventType = xpp.next();
                                }

                                } catch (Exception e) {
                                e.printStackTrace();
                                callback.onFail(e.getMessage());
                                }
                                callback.onSuccess();
                                }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateDeliveryByItem(String docEntry, String date, String itemCode, String quantity, String realQuantity, final CommonCallback callback) {
        String link = String.format(getURL() + Config.UPDATEDELIVERYBYITEM);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", getUserCode());
        params.add(param);

        param = new Param("DocEntry", docEntry);
        params.add(param);

        param = new Param("DeliveryDate_Act", date);
        params.add(param);

        param = new Param("NotesDelivery", "");
        params.add(param);

        param = new Param("ItemCode", itemCode);
        params.add(param);

        param = new Param("Quantity", quantity);
        params.add(param);

        param = new Param("Quantity_Del", realQuantity);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("Alert")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                if (data.contains("UnSuccessfully")) {
                                    callback.onFail(data);
                                    return;
                                }

                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getItemTypes(final ItemTypeCallback callback) {
        String link = String.format(getURL() + Config.GETITEMTYPE);
        List<Param> params = new ArrayList<>();

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<ItemType> itemTypeList = new ArrayList<>();
                try {
                    boolean readyToGetStatus = false;
                    boolean readyToGetName = false;


                    String currentStatus = null;
                    String currentName = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ItemType")) {
                                readyToGetStatus = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("OrderTypeName")) {
                                readyToGetName = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetStatus) {
                                currentStatus = data;
                                readyToGetStatus = false;
                                if (!TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentName)) {
                                    ItemType itemType = new ItemType();
                                    itemType.setItemType(currentStatus);
                                    itemType.setOrderTypeName(currentName);

                                    itemTypeList.add(itemType);

                                    currentStatus = null;
                                    currentName = null;
                                }
                            }

                            if (readyToGetName) {
                                currentName = data;
                                readyToGetName = false;
                                if (!TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentName)) {
                                    ItemType itemType = new ItemType();
                                    itemType.setItemType(currentStatus);
                                    itemType.setOrderTypeName(currentName);

                                    itemTypeList.add(itemType);

                                    currentStatus = null;
                                    currentName = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess(itemTypeList);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void createOrder(OrderToCreate order, final CreateOrderCallback callback) {
        String link = String.format(getURL() + Config.CREATEORDER);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CardCode", order.cardCode);
        params.add(param);
        param = new Param("OrderType", order.orderType);
        params.add(param);
        param = new Param("PODate", order.poDate);
        params.add(param);
        param = new Param("DeliveryDate", order.deliveryDate);
        params.add(param);
        param = new Param("SalesEmp", order.salesEmp);
        params.add(param);
        param = new Param("DocTotal", order.docTotal);
        params.add(param);
        param = new Param("VATTotal", order.vatTotal);
        params.add(param);
        param = new Param("GTotal", order.GTotal);
        params.add(param);
        param = new Param("Remark", order.remarks);
        params.add(param);
        param = new Param("UserCode", getUserCode());
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("DocEntry")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                callback.onSuccess(data);
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onFail("");
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }
    public void addProductToOrder(ProductToAdd product, final CommonCallback callback) {
        String link = String.format(getURL() + Config.ADDPRODUCTTOORDER);

        List<Param> params = new ArrayList<>();
        Param param = new Param("DocEntry", product.docEntry);
        params.add(param);
        param = new Param("ItemCode", product.itemCode);
        params.add(param);
        param = new Param("ItemName", product.itemName);
        params.add(param);
        param = new Param("Quantity", product.quantity);
        params.add(param);
        param = new Param("UoM", product.uoM);
        params.add(param);
        param = new Param("Price", product.price);
        params.add(param);
        param = new Param("Discount", product.discount);
        params.add(param);
        param = new Param("PriceAfterDiscount", product.priceAfterDiscount);
        params.add(param);
        param = new Param("LineTotal", product.lineTotal);
        params.add(param);
        param = new Param("VATPercent", product.vatPercent);
        params.add(param);
        param = new Param("VATAmt", product.vatAmt);
        params.add(param);
        param = new Param("GTotal", product.gTotal);
        params.add(param);
        param = new Param("ItemType", product.itemType);
        params.add(param);
        param = new Param("WhsCode", product.whsCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultString")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                if (data.contains("UnSuccessfully")) {
                                    callback.onFail(data);
                                    return;
                                }

                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }
    public void finishOrder(String docEntry, final CommonCallback callback) {
        String link = String.format(getURL() + Config.FINISHORDER);

        List<Param> params = new ArrayList<>();
        Param param = new Param("DocEntry", docEntry);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultString")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                if (data.contains("UnSuccessfully")) {
                                    callback.onFail(data);
                                    return;
                                }

                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }
}
