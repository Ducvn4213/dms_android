package com.luan.dms_management.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.luan.dms_management.R;
import com.luan.dms_management.adapter.ProductOrderAdapter;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.OrderbyDocForShip;
import com.luan.dms_management.models.Orders;
import com.luan.dms_management.models.Product;
import com.luan.dms_management.models.TypeOrder;
import com.luan.dms_management.models.WareHouse;
import com.luan.dms_management.reaml.RealmController;
import com.luan.dms_management.service.ApiService;
import com.luan.dms_management.service.BasicService;
import com.luan.dms_management.utils.BaseActivity;
import com.luan.dms_management.utils.CommonUtils;
import com.luan.dms_management.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnItemSelected;
import io.realm.Realm;

public class OrderDetail extends BaseActivity {

    @BindView(R.id.orderdetail_cus)
    protected TextView txtName;
    @BindView(R.id.orderdetail_addr)
    protected TextView txtAddress;
    @BindView(R.id.orderdetail_contact)
    protected TextView txtContact;
    @BindView(R.id.orderdetail_phone)
    protected TextView txtPhone;
    @BindView(R.id.orderdetail_cast)
    protected TextView txtCast;
    @BindView(R.id.orderdetail_dateship)
    protected TextView txtDateShip;
    @BindView(R.id.orderdetail_typeorder)
    protected Spinner typeOrder;
    @BindView(R.id.orderdetail_warehouse)
    protected Spinner wareHouse;
    @BindView(R.id.orderdetail_discount)
    protected TextView txtDiscount;
    @BindView(R.id.orderdetail_note)
    protected TextView txtNote;
    @BindView(R.id.lv_product_order)
    protected ListView listView;
    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView navigationView;

    private ProductOrderAdapter adapter;

    private String[] strings;
    private ApiService mService;

    private ArrayList<String> listOrderType, listWareHouse;
    private ArrayList<String> listOrderTypeValue, listWareHouseValue;
    int posWareHouse, posTypeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        ButterKnife.bind(this);
        strings = getResources().getStringArray(R.array.planets_array);
        mService = ApiService.getInstance(this);
        downloadData();
        configControlEvent();
    }

    private void loadData(ArrayList<OrderbyDoc> data) {
        BasicService.orderbyDocs = data;
        adapter = new ProductOrderAdapter(this, data);
        adapter.setViewOnly(true);
        listView.setAdapter(adapter);
        CommonUtils.setListViewHeightBasedOnChildrenCart(this, listView);

        BasicService.currentCus = RealmController.with(this).getCustomer(BasicService.orders.getCardCode());

        getSupportActionBar().setTitle(BasicService.currentCus.getCardName());
        txtName.setText(BasicService.orders.getCardName());
        txtAddress.setText(BasicService.orders.getAddress());
        txtContact.setText(BasicService.currentCus.getContactPerson());
        txtPhone.setText(BasicService.currentCus.getTel());
        txtCast.setText(BasicService.orders.getDocTotal());
        txtDateShip.setText(CommonUtils.formatDate(BasicService.orders.getDeliveryDate()));
        txtNote.setText(BasicService.orders.getRemark());
        loadOrderType();
        loadWareHouse();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                OrderDetail.this, android.R.layout.simple_spinner_item, listWareHouseValue);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wareHouse.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(
                OrderDetail.this, android.R.layout.simple_spinner_item, listOrderTypeValue);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeOrder.setAdapter(adapter1);

        wareHouse.setSelection(0);
        typeOrder.setSelection(Integer.parseInt(BasicService.orders.getOrderType()));

    }

    private void downloadData() {
        showLoading();
        mService.loadListOrderbyDoc(BasicService.orders.getDocEntry(), new ApiService.GetListOrderbyDoc() {
            @Override
            public void onSuccess(final ArrayList<OrderbyDoc> data) {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        try {
                            loadData(data);
                        } catch (Exception e) {
                            showErrorDialog(e.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onFail(final String error) {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        if (error.equalsIgnoreCase("")) {
                            showErrorDialog(getString(R.string.load_list_order));
                        } else {
                            showErrorDialog(error);
                        }
                    }
                });
            }
        });
    }

    private void loadOrderType() {
        listOrderType = new ArrayList<>();
        listOrderTypeValue = new ArrayList<>();
        for (TypeOrder typeOrder : RealmController.with(this).getTypeOrder()) {
            listOrderType.add(typeOrder.getTypeCode());
            listOrderTypeValue.add(typeOrder.getTypeName());
        }
    }

    private void loadWareHouse() {
        listWareHouse = new ArrayList<>();
        listWareHouseValue = new ArrayList<>();
        for (WareHouse wareHouse : RealmController.with(this).getWareHouse()) {
            listWareHouse.add(wareHouse.getWhsCode());
            listWareHouseValue.add(wareHouse.getWhsName());
        }
    }

    private void configControlEvent() {
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_remove:
                        requestCancelOrder();
                        break;
                    case R.id.action_delivery:
                        requestShip();
                        break;
                }
                return false;
            }
        });
    }

    private void requestShip() {
        List<OrderbyDocForShip> shipItems = adapter.getShipProducts();
        showLoading();
        doUpdateShip(shipItems, new ApiService.CommonCallback() {
            @Override
            public void onSuccess() {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        String title = getString(R.string.app_name);
                        String mess = getString(R.string.update_order_success);
                        final AlertDialog dialog = new AlertDialog.Builder(OrderDetail.this)
                                .setTitle(title)
                                .setMessage(mess)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        OrderDetail.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(String error) {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        String title = getString(R.string.app_name);
                        String mess = getString(R.string.update_order_fail);
                        showDialog(title, mess);
                    }
                });
            }
        });
    }

    private void doUpdateShip(final List<OrderbyDocForShip> items, final ApiService.CommonCallback callback) {
        if (items.size() == 0) {
            callback.onSuccess();
            return;
        }

        final OrderbyDocForShip item = items.get(0);
        doUpdateShipOneItem(item, new ApiService.CommonCallback() {
            @Override
            public void onSuccess() {
                items.remove(0);
                doUpdateShip(items, callback);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    private void doUpdateShipOneItem(OrderbyDocForShip item, final ApiService.CommonCallback callback) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String date = df.format(c.getTime());
        mService.updateDeliveryByItem(item.docEntry, date, item.itemCode, item.quatity, item.realQuantity, callback);
    }

    private void requestCancelOrder() {
        final AlertDialog dialog = new AlertDialog.Builder(OrderDetail.this)
                .setTitle(R.string.app_name)
                .setMessage(R.string.cancel_order_message)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doCancelOrder();
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void doCancelOrder() {
        //showLoading();
        String userCode = mService.getUserCode();
        String docEntry =  BasicService.orders.getDocEntry();

        mService.cancelOrder(userCode, docEntry, new ApiService.CommonCallback() {
            @Override
            public void onSuccess() {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();

                        Realm realm = RealmController.with(OrderDetail.this).getRealm();

                        realm.beginTransaction();
                        Orders order = realm.where(Orders.class).equalTo("DocEntry", BasicService.orders.getDocEntry()).findFirst();
                        if(order != null) {
                            order.setDocStatusName("Huỷ");
                            realm.copyToRealmOrUpdate(order);
                        }
                        realm.commitTransaction();

                        String title = getString(R.string.app_name);
                        String mess = getString(R.string.cancel_order_success);
                        showDialog(title, mess);
                    }
                });
            }

            @Override
            public void onFail(final String error) {
                OrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(error);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.ADD_PRODUCT) {
            //loadData();
        }
    }

    @OnItemSelected(R.id.orderdetail_warehouse)
    void onSelectedWareHouse(int position) {
        posWareHouse = position;
    }

    @OnItemSelected(value = R.id.orderdetail_warehouse, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelectedWarehouse() {
        //posWareHouse = 0;
    }

    @OnItemSelected(R.id.orderdetail_typeorder)
    void onSelectedTypeOrder(int position) {
        posTypeOrder = position;
    }

    @OnItemSelected(value = R.id.orderdetail_typeorder, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onSelectedTypeOrder() {
        //posTypeOrder = 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            CommonUtils.makeToast(this, "ok");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
