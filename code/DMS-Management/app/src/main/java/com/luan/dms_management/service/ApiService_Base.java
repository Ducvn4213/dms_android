package com.luan.dms_management.service;

import android.content.Context;

import com.luan.dms_management.utils.PrefUtils;

import static com.luan.dms_management.service.Config.URL_SAVE_KEY;

/**
 * Created by duc.nguyenv on 25/01/2018.
 */

public class ApiService_Base {

    Network mNetwork = Network.getInstance();
    String userURL = null;
    String mUserCode;
    String mSaleManID;

    public void setUserCode(String data) {
        mUserCode = data;
    }
    public void setSaleManID(String data) { mSaleManID = data; }


    public String getUserCode() {
        return mUserCode;
    }

    public String getSaleManID() {
        return mSaleManID;
    }

    public void saveNewURL(Context context, String newURL) {
        PrefUtils.savePreference(context, URL_SAVE_KEY, newURL);
        userURL = newURL;
//        reasonsData = null;
//        wareHousesData = null;
    }

    public String getURL() {
        if (userURL == null) {
            return Config.HOST;
        }

        return userURL;
    }
}
