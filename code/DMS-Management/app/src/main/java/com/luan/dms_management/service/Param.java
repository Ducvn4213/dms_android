package com.luan.dms_management.service;

/**
 * Created by luan.nt on 8/29/2017.
 */

public class Param {
    public String key;
    public String value;

    public Param(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Param() {
    }
}
