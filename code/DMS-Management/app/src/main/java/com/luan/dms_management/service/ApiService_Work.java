package com.luan.dms_management.service;

import android.text.TextUtils;

import com.luan.dms_management.models.LeaveLog;
import com.luan.dms_management.models.LeaveReason;
import com.luan.dms_management.models.LeaveType;
import com.luan.dms_management.models.TimeSheet;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class ApiService_Work extends ApiService_CheckIn {
    public void getLeaveReason(final GetLeaveReasonCallback callback) {
        String link = String.format(getURL() + Config.LEAVEREASON);

        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<LeaveReason> reasonList = new ArrayList<>();
                try {
                    boolean readyToGetCode = false;
                    boolean readyToGetName = false;

                    String currentCode = null;
                    String currentName = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("LCode")) {
                                readyToGetCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LName")) {
                                readyToGetName = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCode) {
                                currentCode = data;
                                readyToGetCode = false;
                                if (!TextUtils.isEmpty(currentCode) && !TextUtils.isEmpty(currentName)) {
                                    LeaveReason leaveReason = new LeaveReason();
                                    leaveReason.setLcode(currentCode);
                                    leaveReason.setLname(currentName);

                                    reasonList.add(leaveReason);

                                    currentCode = null;
                                    currentName = null;
                                }
                            }

                            if (readyToGetName) {
                                currentName = data;
                                readyToGetName = false;
                                if (!TextUtils.isEmpty(currentCode) && !TextUtils.isEmpty(currentName)) {
                                    LeaveReason leaveReason = new LeaveReason();
                                    leaveReason.setLcode(currentCode);
                                    leaveReason.setLname(currentName);

                                    reasonList.add(leaveReason);

                                    currentCode = null;
                                    currentName = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }

                callback.onSuccess(reasonList);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getLeaveType(final GetLeaveTypeCallback callback) {
        String link = String.format(getURL() + Config.LEAVETYPE);

        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<LeaveType> typeList = new ArrayList<>();
                try {
                    boolean readyToGetCode = false;
                    boolean readyToGetName = false;

                    String currentCode = null;
                    String currentName = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("LCode")) {
                                readyToGetCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LName")) {
                                readyToGetName = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCode) {
                                currentCode = data;
                                readyToGetCode = false;
                                if (!TextUtils.isEmpty(currentCode) && !TextUtils.isEmpty(currentName)) {
                                    LeaveType leaveType = new LeaveType();
                                    leaveType.setLcode(currentCode);
                                    leaveType.setLname(currentName);

                                    typeList.add(leaveType);

                                    currentCode = null;
                                    currentName = null;
                                }
                            }

                            if (readyToGetName) {
                                currentName = data;
                                readyToGetName = false;
                                if (!TextUtils.isEmpty(currentCode) && !TextUtils.isEmpty(currentName)) {
                                    LeaveType leaveType = new LeaveType();
                                    leaveType.setLcode(currentCode);
                                    leaveType.setLname(currentName);

                                    typeList.add(leaveType);

                                    currentCode = null;
                                    currentName = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }

                callback.onSuccess(typeList);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void addLeave(String userCode, String deviceID, String fromDate, String toDate,
                         String leaveType, String reason, String remark, final AddLeaveCallback callback) {
        String link = String.format(getURL() + Config.ADDLEAVE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("DeviceID", deviceID);
        params.add(param);
        param = new Param("FromDate", fromDate);
        params.add(param);
        param = new Param("ToDate", toDate);
        params.add(param);
        param = new Param("LeaveType", leaveType);
        params.add(param);
        param = new Param("Reason", reason);
        params.add(param);
        param = new Param("Reamarks", remark);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetCode = false;

                    String currentCode = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetCode = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCode) {
                                currentCode = data;
                                readyToGetCode = false;
                                if (!TextUtils.isEmpty(currentCode)) {

                                    callback.onSuccess(currentCode);

                                    currentCode = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getLeaveLog(String userCode, final GetLeaveLogCallback callback) {
        String link = String.format(getURL() + Config.LEAVELOG);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                List<LeaveLog> leaveLogs = new ArrayList<>();
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetID = false;
                    boolean readyToGetUserCode = false;
                    boolean readyToGetDeviceID = false;
                    boolean readyToGetFromDate = false;
                    boolean readyToGetToDate = false;
                    boolean readyToGetType = false;
                    boolean readyToGetReason = false;
                    boolean readyToGetRemarks = false;
                    boolean readyToGetStatus = false;
                    boolean readyToGetCreateDate = false;
                    boolean readyToGetApproveDate = false;
                    boolean readyToGetApproveUser = false;

                    String currentID = null;
                    String currentUserCode = null;
                    String currentDeviceID = null;
                    String currentFromDate = null;
                    String currentToDate = null;
                    String currentType = null;
                    String currentReason = null;
                    String currentRemarks = null;
                    String currentStatus = null;
                    String currentCreateDate = null;
                    String currentApproveDate = null;
                    String currentApproveUser = null;


                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("LeaveID")) {
                                readyToGetID = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("UserCode")) {
                                readyToGetUserCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("DeviceID")) {
                                readyToGetDeviceID = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("FromDate")) {
                                readyToGetFromDate = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ToDate")) {
                                readyToGetToDate = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LeaveType")) {
                                readyToGetType = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("Reason")) {
                                readyToGetReason = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("Remarks")) {
                                readyToGetRemarks = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("Status")) {
                                readyToGetStatus = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("CreateDate")) {
                                readyToGetCreateDate = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("ApproveDate")) {
                                readyToGetApproveDate = true;
                            }
                            if (xpp.getName().equalsIgnoreCase("ApproveUser")) {
                                readyToGetApproveUser = true;
                            }


                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();
                            if (readyToGetID) {
                                currentID = data;
                                readyToGetID = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetUserCode) {
                                currentUserCode = data;
                                readyToGetUserCode = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetDeviceID) {
                                currentDeviceID = data;
                                readyToGetDeviceID = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetFromDate) {
                                currentFromDate = data;
                                readyToGetFromDate = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetToDate) {
                                currentToDate = data;
                                readyToGetToDate = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetType) {
                                currentType = data;
                                readyToGetType = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetReason) {
                                currentReason = data;
                                readyToGetReason = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetRemarks) {
                                currentRemarks = data;
                                readyToGetRemarks = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetStatus) {
                                currentStatus = data;
                                readyToGetStatus = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }

                            if (readyToGetCreateDate) {
                                currentCreateDate = data;
                                readyToGetCreateDate = false;
                                if (!TextUtils.isEmpty(currentID) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentFromDate) &&
                                        !TextUtils.isEmpty(currentToDate) && !TextUtils.isEmpty(currentType) &&
                                        !TextUtils.isEmpty(currentReason) && !TextUtils.isEmpty(currentRemarks) &&
                                        !TextUtils.isEmpty(currentStatus) && !TextUtils.isEmpty(currentCreateDate)) {
                                    LeaveLog leaveLog = new LeaveLog();
                                    leaveLog.setLeaveId(currentID);
                                    leaveLog.setUserCode(currentUserCode);
                                    leaveLog.setDeveiceId(currentDeviceID);
                                    leaveLog.setFromDate(currentFromDate);
                                    leaveLog.setToDate(currentToDate);
                                    leaveLog.setLeaveType(currentType);
                                    leaveLog.setReason(currentReason);
                                    leaveLog.setRemark(currentRemarks);
                                    leaveLog.setStatus(currentStatus);
                                    leaveLog.setCreateDate(currentCreateDate);
                                    leaveLog.setApproveDate(currentApproveDate);
                                    leaveLog.setApproveUser(currentApproveUser);

                                    leaveLogs.add(leaveLog);

                                    currentID = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentFromDate = null;
                                    currentToDate = null;
                                    currentType = null;
                                    currentReason = null;
                                    currentRemarks = null;
                                    currentStatus = null;
                                    currentCreateDate = null;
                                    currentApproveDate = null;
                                    currentApproveUser = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess(leaveLogs);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void approveLeave(String leaveID, final ApproveLeaveCallback callback) {
        String link = String.format(getURL() + Config.LEAVEAPPROVE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("LeaveID", leaveID);
        params.add(param);
        param = new Param("USerCode", getUserCode());
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetCode = false;

                    String currentCode = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetCode = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();
                            callback.onSuccess("success");
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void addTimeSheet(String userCode, String deviceID, String lat, String lang, final AddTimeSheetCallback callback) {
        String link = String.format(getURL() + Config.ADDTIMESHEET);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("DeviceID", deviceID);
        params.add(param);
        param = new Param("LatitudeValue_Start", lat);
        params.add(param);
        param = new Param("LongitudeValue_Start", lang);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetCode = false;

                    String currentCode = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetCode = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCode) {
                                currentCode = data;
                                readyToGetCode = false;
                                if (!TextUtils.isEmpty(currentCode)) {

                                    callback.onSuccess(currentCode);
                                    return;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateTimeSheet(String timeID, String lat, String lang, final UpdateTimeSheetCallback callback) {
        String link = String.format(getURL() + Config.UPDATETIMESHEET);

        List<Param> params = new ArrayList<>();
        Param param = new Param("TSId", timeID);
        params.add(param);
        param = new Param("LatitudeValue_End", lat);
        params.add(param);
        param = new Param("LongitudeValue_end", lang);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetCode = false;

                    String currentCode = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetCode = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetCode) {
                                currentCode = data;
                                readyToGetCode = false;
                                if (!TextUtils.isEmpty(currentCode) && currentCode.equals("1")) {

                                    callback.onSuccess(currentCode);

                                    currentCode = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getTimeSheetLog(String userCode, final LogTimeSheetCallback callback) {
        String link = String.format(getURL() + Config.TIMESHEETLOG);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                List<TimeSheet> sheetList = new ArrayList<>();
                try {
                    boolean readyToGetTSid = false;
                    boolean readyToGetUserCode = false;
                    boolean readyToGetDeviceID = false;
                    boolean readyToGetStartTime = false;
                    boolean readyToGetLatStart = false;
                    boolean readyToGetLongStart = false;
                    boolean readyToGetEndTime = false;
                    boolean readyToGetLatEnd = false;
                    boolean readyToGetLongEnd = false;

                    String currentTSid = null;
                    String currentUserCode = null;
                    String currentDeviceID = null;
                    String currentStartTime = null;
                    String currentLatStart = null;
                    String currentLongStart = null;
                    String currentEndTime = null;
                    String currentLatEnd = null;
                    String currentLongEnd = null;

                    boolean isWaitForEnd = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("TSId")) {
                                readyToGetTSid = true;

                                if (isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);
                                }

                                isWaitForEnd = true;
                                currentTSid = null;
                                currentUserCode = null;
                                currentDeviceID = null;
                                currentStartTime = null;
                                currentEndTime = null;
                                isWaitForEnd = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("UserCode")) {
                                readyToGetUserCode = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("DeviceID")) {
                                readyToGetDeviceID = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("StartTime")) {
                                readyToGetStartTime = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LatitudeValue_Start")) {
                                readyToGetLatStart = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LongitudeValue_Start")) {
                                readyToGetLongStart = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("EndTime")) {
                                readyToGetEndTime = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LatitudeValue_End")) {
                                readyToGetLatEnd = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("LongitudeValue_end")) {
                                readyToGetLongEnd = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetTSid) {
                                currentTSid = data;
                                readyToGetTSid = false;
                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            }

                            if (readyToGetUserCode) {
                                currentUserCode = data;
                                readyToGetUserCode = false;
                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            }

                            if (readyToGetDeviceID) {
                                currentDeviceID = data;
                                readyToGetDeviceID = false;
                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            }

                            if (readyToGetStartTime) {
                                currentStartTime = data;
                                readyToGetStartTime = false;
                                isWaitForEnd = true;
                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            }

                            if (readyToGetEndTime) {
                                currentEndTime = data;
                                readyToGetEndTime = false;

                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);
                                    timeSheet.setEndtime(currentEndTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            } else {
                                if (!TextUtils.isEmpty(currentTSid) && !TextUtils.isEmpty(currentUserCode) &&
                                        !TextUtils.isEmpty(currentDeviceID) && !TextUtils.isEmpty(currentStartTime) && !isWaitForEnd) {
                                    TimeSheet timeSheet = new TimeSheet();
                                    timeSheet.setTsid(currentTSid);
                                    timeSheet.setUsercode(currentUserCode);
                                    timeSheet.setDeviceid(currentDeviceID);
                                    timeSheet.setStarttime(currentStartTime);

                                    sheetList.add(timeSheet);

                                    currentTSid = null;
                                    currentUserCode = null;
                                    currentDeviceID = null;
                                    currentStartTime = null;
                                    currentEndTime = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                    if (isWaitForEnd) {
                        TimeSheet timeSheet = new TimeSheet();
                        timeSheet.setTsid(currentTSid);
                        timeSheet.setUsercode(currentUserCode);
                        timeSheet.setDeviceid(currentDeviceID);
                        timeSheet.setStarttime(currentStartTime);
                        timeSheet.setEndtime(currentEndTime);

                        sheetList.add(timeSheet);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }

                callback.onSuccess(sheetList);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateLocation(String userCode, String lat, String lon, String battery, String deviceID) {
        String link = String.format(getURL() + Config.UPDATELOCATION);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("CurrLat", lat);
        params.add(param);
        param = new Param("CurrLong", lon);
        params.add(param);
        param = new Param("CurrBatteryPercent", battery);
        params.add(param);
        param = new Param("DeviceID", deviceID);
        params.add(param);


        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
//                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
//                callback.onFail(error);
            }
        });
    }
}
