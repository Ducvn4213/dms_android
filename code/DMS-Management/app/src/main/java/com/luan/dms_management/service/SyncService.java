package com.luan.dms_management.service;

import android.content.Context;

import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luan.dms_management.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import static com.luan.dms_management.service.Config.CHECKIN;


public class SyncService extends SyncService_OffineWork {
    private static SyncService instance;
    private SyncService(Context context) {
        mContext = context;
    }

    public static SyncService getInstance(Context context) {
        if (instance == null) {
            instance = new SyncService(context);
        }

        return instance;
    }


    public void sync(final ApiService.CommonCallback callback) {
        processSyncWith(callback);
    }

    private void processSyncWith(final ApiService.CommonCallback callback) {
        final List<SyncService_OffineWork.SyncObject> data = getSyncObjectsFromSP().getData();
        if (data.size() == 0) {
            callback.onSuccess();
            return;
        }

        SyncService_OffineWork.SyncObject syncObject = data.get(0);
        processSyncWith(syncObject, new ApiService.CommonCallback() {
            @Override
            public void onSuccess() {
                data.remove(0);

                SyncObjects s = new SyncObjects();
                s.addSyncObjects(data);
                saveSyncObjectsToSP(s);
                processSyncWith(callback);
            }

            @Override
            public void onFail(String error) {
                callback.onFail("");
            }
        });
    }

    private void processSyncWith(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        if (object.name.equalsIgnoreCase(CHECKIN)) {
            doCheckin(object, callback);
            return;
        }

        if (object.name.equalsIgnoreCase(CHECKIN_IN_ADD_IMAGE)) {
            doCheckinInStockAddImage(object, callback);
            return;
        }

        if (object.name.equalsIgnoreCase(CHECKIN_IN_STOCK)) {
            doCheckinInStock(object, callback);
            return;
        }

        if (object.name.equalsIgnoreCase(CHECKIN_NOTE)) {
            doCheckinNote(object, callback);
            return;
        }

        if (object.name.equalsIgnoreCase(CHECKOUT)) {
            doCheckOutNote(object, callback);
        }

        if (object.name.equalsIgnoreCase(ADD_ORDER)) {
            doAddOrder2(object, callback);
        }
    }

    private void doCheckin(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        String customerCode = null;
        String userCode = null;
        String deviceID = null;

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("CustCode")) {
                customerCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("UserCode")) {
                userCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("DeviceID")) {
                deviceID = param.value;
            }
        }

        if (customerCode == null || userCode == null || deviceID == null) {
            callback.onFail("");
            return;
        }

        ApiService.getInstance(mContext).checkin(customerCode, userCode, deviceID, new ApiService.CheckinCallback() {
            @Override
            public void onSuccess(String checkinID) {
                PrefUtils.savePreference(mContext, CHECKIN_ID_PREF, checkinID);
                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    private void doCheckinInStock(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        String customerCode = null;
        String userCode = null;
        String checkInID = PrefUtils.getPreference(mContext, CHECKIN_ID_PREF);
        String itemCode = null;
        String barCode = null;
        String caseInStock = null;
        String bottleInStock = null;
        String remark = null;

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("CustCode")) {
                customerCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("UserCode")) {
                userCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("ItemCode")) {
                itemCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("BarCode")) {
                barCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("CaseInStock")) {
                caseInStock = param.value;
            }
            else if (param.key.equalsIgnoreCase("BottleInStock")) {
                bottleInStock = param.value;
            }
            else if (param.key.equalsIgnoreCase("Remarks")) {
                remark = param.value;
            }
        }

        ApiService.getInstance(mContext).checkinInstock(customerCode, userCode, checkInID, itemCode, barCode, caseInStock, bottleInStock, remark, callback);
    }

    private void doCheckinNote(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        String customerCode = null;
        String userCode = null;
        String checkInID = PrefUtils.getPreference(mContext, CHECKIN_ID_PREF);
        String remark = null;
        String notesGroup = null;

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("CustCode")) {
                customerCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("UserCode")) {
                userCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("NotesGroup")) {
                notesGroup = param.value;
            }
            else if (param.key.equalsIgnoreCase("NotesRemark")) {
                remark = param.value;
            }
        }

        ApiService.getInstance(mContext).checkinAddLog(customerCode, userCode, checkInID, notesGroup, remark, callback);
    }

    private void doCheckOutNote(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        String customerCode = null;
        String userCode = null;
        String checkInID = PrefUtils.getPreference(mContext, CHECKIN_ID_PREF);

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("CustCode")) {
                customerCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("UserCode")) {
                userCode = param.value;
            }
        }

        ApiService.getInstance(mContext).checkout(customerCode, userCode, checkInID, callback);
    }

    private void doAddOrder2(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        ApiService_ProductAndOrder.OrderToCreate orderToCreate = null;
        List<ApiService_ProductAndOrder.ProductToAdd> productToAddList = null;

        Gson gson = new Gson();

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("OrderToCreate")) {
                orderToCreate = gson.fromJson(param.value, ApiService_ProductAndOrder.OrderToCreate.class);
            }
            else if (param.key.equalsIgnoreCase("ProductToAddList")) {
                productToAddList = gson.fromJson(param.value, new TypeToken<List<ApiService_ProductAndOrder.ProductToAdd>>(){}.getType());
            }
        }

        final List<ApiService_ProductAndOrder.ProductToAdd> theProductToAddList = productToAddList == null ? new ArrayList<ApiService_ProductAndOrder.ProductToAdd>() : productToAddList;

        ApiService.getInstance(mContext).createOrder(orderToCreate, new ApiService_CallbackDefine.CreateOrderCallback() {
            @Override
            public void onSuccess(final String data) {
                for (ApiService_ProductAndOrder.ProductToAdd p : theProductToAddList) {
                    p.docEntry = data;
                }

                doAddProductToOrder(theProductToAddList, new ApiService_CallbackDefine.CommonCallback() {
                    @Override
                    public void onSuccess() {
                        ApiService.getInstance(mContext).finishOrder(data, callback);
                    }

                    @Override
                    public void onFail(String error) {
                        callback.onFail(error);
                    }
                });
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    private void doAddProductToOrder(final List<ApiService_ProductAndOrder.ProductToAdd> productToAddList, final ApiService_CallbackDefine.CommonCallback callback) {
        if (productToAddList.size() == 0) {
            callback.onSuccess();
            return;
        }

        ApiService_ProductAndOrder.ProductToAdd productToAdd = productToAddList.get(0);
        ApiService.getInstance(mContext).addProductToOrder(productToAdd, new ApiService_CallbackDefine.CommonCallback() {
            @Override
            public void onSuccess() {
                productToAddList.remove(0);
                doAddProductToOrder(productToAddList, callback);
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    private void doCheckinInStockAddImage(SyncService_OffineWork.SyncObject object, final ApiService.CommonCallback callback) {
        String customerCode = null;
        String userCode = null;
        String checkInID = PrefUtils.getPreference(mContext, CHECKIN_ID_PREF);
        String pic1 = "";
        String pic2 = "";
        String pic3 = "";

        for (SyncParam param : object.getParamsData()) {
            if (param.key.equalsIgnoreCase("CustCode")) {
                customerCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("UserCode")) {
                userCode = param.value;
            }
            else if (param.key.equalsIgnoreCase("Pics1")) {
                pic1 = param.value;
            }
            else if (param.key.equalsIgnoreCase("Pics2")) {
                pic2 = param.value;
            }
            else if (param.key.equalsIgnoreCase("Pics3")) {
                pic3 = param.value;
            }
        }

        ApiService.getInstance(mContext).checkInAddPicture(checkInID, customerCode, userCode, pic1, pic2, pic3, callback);
    }
}
