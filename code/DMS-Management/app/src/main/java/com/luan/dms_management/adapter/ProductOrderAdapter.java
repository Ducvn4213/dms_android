package com.luan.dms_management.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.luan.dms_management.R;
import com.luan.dms_management.activities.EditCustomer;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.OrderbyDocForShip;
import com.luan.dms_management.models.Product;
import com.luan.dms_management.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by luan.nt on 8/1/2017.
 */

public class ProductOrderAdapter extends BaseAdapter {
    private Context context;
    private List<OrderbyDoc> items;
    List<OrderbyDocForShip> shipItems;
    private boolean isViewOnly = false;

    public ProductOrderAdapter(Context context, List<OrderbyDoc> items) {
        this.context = context;
        this.items = items;
        initShipItem();
    }

    private void initShipItem() {
        shipItems = new ArrayList<>();
        for (OrderbyDoc o : items) {
            OrderbyDocForShip orderbyDocForShip = new OrderbyDocForShip();
            orderbyDocForShip.itemCode = o.getItemCode();
            orderbyDocForShip.docEntry = o.getDocEntry();
            orderbyDocForShip.quatity = o.getQuantity();
            orderbyDocForShip.realQuantity = o.getQuantity();

            shipItems.add(orderbyDocForShip);
        }
    }

    public void setViewOnly(boolean viewOnly) {
        this.isViewOnly = viewOnly;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LinearLayout layout = (LinearLayout) convertView;
        if (convertView != null) {
            holder = (ViewHolder) layout.getTag();
        } else {
            layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.item_product_order, null);
            holder = new ViewHolder(layout);
            layout.setTag(holder);
        }

        holder.txtProduct.setText(items.get(position).getItemName());
        holder.txtDiscountPro.setText(items.get(position).getDiscount());
        holder.txtUnitPrice.setText(items.get(position).getPrice());
        holder.txtQuantity.setText(items.get(position).getQuantity());
        holder.edtDeliveryQuantity.setText(shipItems.get(position).realQuantity);
        holder.Unit.setText(items.get(position).getUoM());

        holder.setViewOnly(isViewOnly);
        holder.configShipItem(position);

        return layout;
    }

    public List<OrderbyDocForShip> getShipProducts() {
        return shipItems;
    }


    public class ViewHolder {
        @BindView(R.id.item_order_product)
        protected TextView txtProduct;
        @BindView(R.id.item_order_unit)
        protected TextView Unit;
        @BindView(R.id.item_order_unitprice)
        protected TextView txtUnitPrice;
        @BindView(R.id.item_order_discountPro)
        protected TextView txtDiscountPro;
        @BindView(R.id.item_order_quantity)
        protected TextView txtQuantity;
        @BindView(R.id.item_order_product_delete)
        protected TextView icdelete;
        @BindView(R.id.edt_delivery_quantity)
        protected EditText edtDeliveryQuantity;



        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
            CommonUtils.makeTextViewFont(context, icdelete);
        }

        public void setViewOnly(boolean viewOnly) {
            if (viewOnly) {
                icdelete.setVisibility(View.GONE);
                edtDeliveryQuantity.setVisibility(View.VISIBLE);
                return;
            }

            icdelete.setVisibility(View.VISIBLE);
            edtDeliveryQuantity.setVisibility(View.GONE);
        }

        public void configShipItem(final int position) {
            edtDeliveryQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //TODO: do nothing
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        Double newQuantity = Double.parseDouble(s.toString());
                        shipItems.get(position).realQuantity = newQuantity.toString();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        shipItems.get(position).realQuantity = "0";
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    //TODO: do nothing
                }
            });
        }
    }
}
