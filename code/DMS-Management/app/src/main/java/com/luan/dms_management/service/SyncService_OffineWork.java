package com.luan.dms_management.service;

import android.content.Context;

import com.google.gson.Gson;
import com.luan.dms_management.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

public class SyncService_OffineWork {
    protected static final String CHECKIN = "CHECKIN";
    protected static final String CHECKIN_NOTE = "CHECKIN_NOTE";
    protected static final String CHECKIN_IN_STOCK = "CHECKIN_IN_STOCK";
    protected static final String CHECKIN_IN_ADD_IMAGE = "CHECKIN_IN_ADD_IMAGE";
    protected static final String CHECKOUT = "CHECKOUT";
    protected static final String ADD_ORDER = "ADD_ORDER";
    protected static final String CHECKIN_ID_PREF = "CHECKIN_ID_PREF";
    protected final String SYNC_OBJECTS_PREF = "SYNC_OBJECTS_PREF";

    protected Context mContext;

    public static class SyncObjects {
        private List<SyncObject> data;

        public SyncObjects() {
            data = new ArrayList<>();
        }

        public void addSyncObject(SyncObject object) {
            data.add(object);
        }

        public void addSyncObjects(List<SyncObject> objects) {
            data.addAll(objects);
        }

        public List<SyncObject> getData() {
            return data;
        }
    }
    public static class SyncObject {
        public String name;
        private List<SyncParam> paramsData;

        public SyncObject() {
            paramsData = new ArrayList<>();
        }

        public void addParam(SyncParam param) {
            paramsData.add(param);
        }

        public void addParams(List<SyncParam> params) {
            paramsData.addAll(params);
        }

        public List<SyncParam> getParamsData() {
            return paramsData;
        }
    }
    public static class SyncParam {
        public String key;
        public String value;

        public SyncParam() {}
    }

    protected SyncService_OffineWork.SyncObjects getSyncObjectsFromSP() {
        String data = PrefUtils.getPreference(mContext, SYNC_OBJECTS_PREF);
        if (data == null || data.isEmpty()) {
            return new SyncObjects();
        }

        Gson gson = new Gson();

        SyncObjects returnData = gson.fromJson(data, SyncObjects.class);
        return returnData;
    }
    protected void saveSyncObjectsToSP(SyncService_OffineWork.SyncObjects data) {
        Gson gson = new Gson();
        String jsonData = gson.toJson(data);

        PrefUtils.savePreference(mContext, SYNC_OBJECTS_PREF, jsonData);
    }

    protected void addSyncObject(SyncService_OffineWork.SyncObject data) {
        SyncService_OffineWork.SyncObjects rootData = getSyncObjectsFromSP();
        rootData.addSyncObject(data);
        saveSyncObjectsToSP(rootData);
    }
    protected void addSyncObjects(List<SyncService_OffineWork.SyncObject> data) {
        SyncService_OffineWork.SyncObjects rootData = getSyncObjectsFromSP();
        rootData.addSyncObjects(data);
        saveSyncObjectsToSP(rootData);
    }

    public void offlineCheckin(String customerCode, String userCode, String deviceID) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = CHECKIN;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramCustomerCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramUserCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramDeviceID = new SyncService_OffineWork.SyncParam();

        paramCustomerCode.key = "CustCode";
        paramCustomerCode.value = customerCode;

        paramUserCode.key = "UserCode";
        paramUserCode.value = userCode;

        paramDeviceID.key = "DeviceID";
        paramDeviceID.value = deviceID;

        params.add(paramCustomerCode);
        params.add(paramUserCode);
        params.add(paramDeviceID);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }

    public void offlineCheckinAddNote(String customerCode, String userCode, String notesGroup, String notesRemark) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = CHECKIN_NOTE;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramCustomerCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramUserCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramNotesGroup = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramNotesRemark = new SyncService_OffineWork.SyncParam();

        paramCustomerCode.key = "CustCode";
        paramCustomerCode.value = customerCode;

        paramUserCode.key = "UserCode";
        paramUserCode.value = userCode;

        paramNotesGroup.key = "NotesGroup";
        paramNotesGroup.value = notesGroup;

        paramNotesRemark.key = "NotesRemark";
        paramNotesRemark.value = notesRemark;

        params.add(paramCustomerCode);
        params.add(paramUserCode);
        params.add(paramNotesGroup);
        params.add(paramNotesRemark);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }

    public void offlineCheckinInStockAdd(String customerCode, String userCode, String itemCode, String barCode, String caseInStock, String bottleInStock, String remarks) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = CHECKIN_IN_STOCK;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramCustomerCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramUserCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramItemCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramBarCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramCaseInStock = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramBottleInStock = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramRemark = new SyncService_OffineWork.SyncParam();

        paramCustomerCode.key = "CustCode";
        paramCustomerCode.value = customerCode;

        paramUserCode.key = "UserCode";
        paramUserCode.value = userCode;

        paramItemCode.key = "ItemCode";
        paramItemCode.value = itemCode;

        paramBarCode.key = "BarCode";
        paramBarCode.value = barCode;

        paramCaseInStock.key = "CaseInStock";
        paramCaseInStock.value = caseInStock;

        paramBottleInStock.key = "BottleInStock";
        paramBottleInStock.value = bottleInStock;

        paramRemark.key = "Remarks";
        paramRemark.value = remarks;


        params.add(paramCustomerCode);
        params.add(paramUserCode);
        params.add(paramItemCode);
        params.add(paramBarCode);
        params.add(paramCaseInStock);
        params.add(paramBottleInStock);
        params.add(paramRemark);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }

    public void offlineCheckout(String customerCode, String userCode) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = CHECKOUT;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramCustomerCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramUserCode = new SyncService_OffineWork.SyncParam();

        paramCustomerCode.key = "CustCode";
        paramCustomerCode.value = customerCode;

        paramUserCode.key = "UserCode";
        paramUserCode.value = userCode;

        params.add(paramCustomerCode);
        params.add(paramUserCode);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }

    public void offlineAddOrderProduct(ApiService_ProductAndOrder.OrderToCreate orderToCreate, List<ApiService_ProductAndOrder.ProductToAdd> productToAddList) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = ADD_ORDER;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramOrderToCreate = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramProductToAddList = new SyncService_OffineWork.SyncParam();

        Gson gson = new Gson();

        paramOrderToCreate.key = "OrderToCreate";
        paramOrderToCreate.value = gson.toJson(orderToCreate);

        paramProductToAddList.key = "ProductToAddList";
        paramProductToAddList.value = gson.toJson(productToAddList);

        params.add(paramOrderToCreate);
        params.add(paramProductToAddList);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }

    public void offlineCheckInStockAddImage(String customerCode, String userCode, String pic1, String pic2, String pic3) {
        SyncService_OffineWork.SyncObject syncObject = new SyncService_OffineWork.SyncObject();
        syncObject.name = CHECKIN_IN_ADD_IMAGE;

        List<SyncService_OffineWork.SyncParam> params = new ArrayList<>();
        SyncService_OffineWork.SyncParam paramCustomerCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramUserCode = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramPic1 = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramPic2 = new SyncService_OffineWork.SyncParam();
        SyncService_OffineWork.SyncParam paramPic3 = new SyncService_OffineWork.SyncParam();

        paramCustomerCode.key = "CustCode";
        paramCustomerCode.value = customerCode;

        paramUserCode.key = "UserCode";
        paramUserCode.value = userCode;

        paramPic1.key = "Pics1";
        paramPic1.value = pic1;

        paramPic2.key = "Pics2";
        paramPic2.value = pic2;

        paramPic3.key = "Pics3";
        paramPic3.value = pic3;


        params.add(paramCustomerCode);
        params.add(paramUserCode);
        params.add(paramPic1);
        params.add(paramPic2);
        params.add(paramPic3);

        syncObject.addParams(params);

        addSyncObject(syncObject);
    }
}
