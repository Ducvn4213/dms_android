package com.luan.dms_management.service;

import android.content.Context;
import android.text.TextUtils;

import com.luan.dms_management.models.Channel;
import com.luan.dms_management.models.Customer;
import com.luan.dms_management.models.CustomerGroup;
import com.luan.dms_management.models.DocStatus;
import com.luan.dms_management.models.InventoryProduct;
import com.luan.dms_management.models.ItemType;
import com.luan.dms_management.models.LeaveLog;
import com.luan.dms_management.models.LeaveReason;
import com.luan.dms_management.models.LeaveType;
import com.luan.dms_management.models.NormalNoteType;
import com.luan.dms_management.models.NormalProduct;
import com.luan.dms_management.models.NoteType;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.Orders;
import com.luan.dms_management.models.Product;
import com.luan.dms_management.models.PromotionProduct;
import com.luan.dms_management.models.Route;
import com.luan.dms_management.models.Staff;
import com.luan.dms_management.models.StaffLocation;
import com.luan.dms_management.models.StaffMonitor;
import com.luan.dms_management.models.Statistic;
import com.luan.dms_management.models.StatusOrder;
import com.luan.dms_management.models.TimeSheet;
import com.luan.dms_management.models.TypeOrder;
import com.luan.dms_management.models.TypeVisit;
import com.luan.dms_management.models.WareHouse;
import com.luan.dms_management.utils.CommonUtils;
import com.luan.dms_management.utils.PrefUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static com.luan.dms_management.service.Config.URL_SAVE_KEY;

/**
 * Created by luan.nt on 4/12/2017.
 */

public class ApiService extends ApiService_ProductAndOrder {
    private static ApiService instance;

    private ApiService(Context context) {
        String oldURL = PrefUtils.getPreference(context, Config.URL_SAVE_KEY);
        if (oldURL != null) {
            userURL = oldURL;
        }
    }

    public static ApiService getInstance(Context context) {
        if (instance == null) {
            instance = new ApiService(context);
        }

        return instance;
    }

    public void login(String username, String password, String deviceID, final LoginCallBack callback) {
        String link = String.format(getURL() + Config.LOGIN);

        mUserCode = username;
        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", username);
        params.add(param);
        param = new Param("Password", password);
        params.add(param);
        param = new Param("DeviceID", deviceID);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;
                    boolean readyToGetUser = false;
                    boolean readyToGetWorking = false;
                    boolean readyToGetSaleID = false;
                    boolean readyToGetNoSec = false;

                    String currentResult = null;
                    String currentUser = null;
                    String currentWorking = null;
                    String currentSaleID = null;
                    String currentNoSec = null;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("Result")) {
                                readyToGetResult = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("UserCode")) {
                                readyToGetUser = true;
                            }

                            if (xpp.getName().equalsIgnoreCase("WorkingDate")) {
                                readyToGetWorking = true;
                            }
                            if (xpp.getName().equalsIgnoreCase("SalesManId")) {
                                readyToGetSaleID = true;
                            }
                            if (xpp.getName().equalsIgnoreCase("NoSecRefresh")) {
                                readyToGetNoSec = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                currentResult = data;
                                readyToGetResult = false;
                                if (!TextUtils.isEmpty(currentResult) &&
                                        !TextUtils.isEmpty(currentUser) &&
                                        !TextUtils.isEmpty(currentWorking) &&
                                        !TextUtils.isEmpty(currentNoSec) &&
                                        !TextUtils.isEmpty(currentSaleID)) {
                                    if (currentResult.equalsIgnoreCase("1")) {
                                        callback.onSuccess(currentWorking, currentSaleID, currentNoSec);
                                    } else {
                                        callback.onFail("");
                                    }

                                    currentResult = null;
                                    currentUser = null;
                                    currentWorking = null;
                                    currentSaleID = null;
                                    currentNoSec = null;
                                }
                            }

                            if (readyToGetUser) {
                                currentUser = data;
                                readyToGetUser = false;
                                if (!TextUtils.isEmpty(currentResult) &&
                                        !TextUtils.isEmpty(currentUser) &&
                                        !TextUtils.isEmpty(currentWorking) &&
                                        !TextUtils.isEmpty(currentNoSec) &&
                                        !TextUtils.isEmpty(currentSaleID)) {
                                    if (currentResult.equalsIgnoreCase("1")) {
                                        callback.onSuccess(currentWorking, currentSaleID, currentNoSec);
                                    } else {
                                        callback.onFail("");
                                    }

                                    currentResult = null;
                                    currentUser = null;
                                    currentWorking = null;
                                    currentSaleID = null;
                                    currentNoSec = null;
                                }
                            }

                            if (readyToGetWorking) {
                                currentWorking = data;
                                readyToGetWorking = false;
                                if (!TextUtils.isEmpty(currentResult) &&
                                        !TextUtils.isEmpty(currentUser) &&
                                        !TextUtils.isEmpty(currentWorking) &&
                                        !TextUtils.isEmpty(currentNoSec) &&
                                        !TextUtils.isEmpty(currentSaleID)) {
                                    if (currentResult.equalsIgnoreCase("1")) {
                                        callback.onSuccess(currentWorking, currentSaleID, currentNoSec);
                                    } else {
                                        callback.onFail("");
                                    }

                                    currentResult = null;
                                    currentUser = null;
                                    currentWorking = null;
                                    currentSaleID = null;
                                    currentNoSec = null;
                                }
                            }

                            if (readyToGetSaleID) {
                                currentSaleID = data;
                                readyToGetSaleID = false;
                                if (!TextUtils.isEmpty(currentResult) &&
                                        !TextUtils.isEmpty(currentUser) &&
                                        !TextUtils.isEmpty(currentWorking) &&
                                        !TextUtils.isEmpty(currentNoSec) &&
                                        !TextUtils.isEmpty(currentSaleID)) {
                                    if (currentResult.equalsIgnoreCase("1")) {
                                        callback.onSuccess(currentWorking, currentSaleID, currentNoSec);
                                    } else {
                                        callback.onFail("");
                                    }

                                    currentResult = null;
                                    currentUser = null;
                                    currentWorking = null;
                                    currentSaleID = null;
                                    currentNoSec = null;
                                }
                            }

                            if (readyToGetNoSec) {
                                currentNoSec = data;
                                readyToGetNoSec = false;
                                if (!TextUtils.isEmpty(currentResult) &&
                                        !TextUtils.isEmpty(currentUser) &&
                                        !TextUtils.isEmpty(currentWorking) &&
                                        !TextUtils.isEmpty(currentNoSec) &&
                                        !TextUtils.isEmpty(currentSaleID)) {
                                    if (currentResult.equalsIgnoreCase("1")) {
                                        callback.onSuccess(currentWorking, currentSaleID, currentNoSec);
                                    } else {
                                        callback.onFail("");
                                    }

                                    currentResult = null;
                                    currentUser = null;
                                    currentWorking = null;
                                    currentSaleID = null;
                                    currentNoSec = null;
                                }
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void changePassword(String userCode, String oldPass, String newPass, String deviceID, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CHANGEPASSWORD);

        List<Param> params = new ArrayList<>();
        Param param = new Param("UserCode", userCode);
        params.add(param);

        param = new Param("OldPass", oldPass);
        params.add(param);

        param = new Param("NewPassword", newPass);
        params.add(param);

        param = new Param("DeviceID", deviceID);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("AlertSystem")) {
                                readyToGetResult = true;
                            }

                        } else if (eventType == XmlPullParser.TEXT) {
                            String data = xpp.getText();

                            if (readyToGetResult) {
                                if (data.contains("UnSuccessfully")) {
                                    callback.onFail(data);
                                    return;
                                }

                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
                callback.onSuccess();
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getNoteFromUser(String customerCode, String userCode, final GetNoteCallback callback) {
        String link = String.format(getURL() + Config.GETNOTE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CardCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("NoteString")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                String data = xpp.getText();
                                callback.onSuccess(data);
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }
}
