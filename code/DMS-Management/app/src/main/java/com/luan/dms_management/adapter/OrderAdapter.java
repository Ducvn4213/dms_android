package com.luan.dms_management.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.luan.dms_management.R;
import com.luan.dms_management.activities.CreateOrderDetail;
import com.luan.dms_management.activities.OrderActivity;
import com.luan.dms_management.activities.OrderDetail;
import com.luan.dms_management.models.DocStatus;
import com.luan.dms_management.models.Orders;
import com.luan.dms_management.reaml.RealmController;
import com.luan.dms_management.service.ApiService;
import com.luan.dms_management.service.BasicService;
import com.luan.dms_management.utils.CommonUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by luan.nt on 7/25/2017.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    private Context context;
    private List<Orders> items;
    private OrderActivity containerInstance;

    public OrderAdapter(Context context, List<Orders> item) {
        this.context = context;
        this.items = item;
    }

    public void setContainerInstance(OrderActivity instance) {
        containerInstance = instance;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Orders item = this.items.get(position);
        holder.id.setText(item.getCardName() + " - " + item.getPONo());
        holder.cost.setText(context.getString(R.string.cost) + " " + item.getDocTotal());
        holder.dateorder.setText(context.getString(R.string.dateorder) + " " + CommonUtils.formatDate(item.getPODate()));
        holder.address.setText(item.getAddress());
        holder.phone.setText(item.getOrderTypeName());

        if (item.getDeliveryDate_Act() == null || item.getDeliveryDate_Act().trim().isEmpty()) {
            holder.phone.setText(context.getString(R.string.order_act_date) + " None");
        }
        else {
            holder.phone.setText(context.getString(R.string.order_act_date) + " " + CommonUtils.formatDate(item.getDeliveryDate_Act()));
        }

        if (item.getNotesDelivery() == null || item.getNotesDelivery().trim().isEmpty()) {
            holder.status.setText(context.getString(R.string.order_note) + " None");
        }
        else {
            holder.status.setText(context.getString(R.string.order_note) + " " + item.getNotesDelivery());
        }

        holder.txtStatusName.setText(context.getString(R.string.status_name) + ": " + item.getDocStatusName());
        holder.txtDeliveryDate.setText(context.getString(R.string.delivery_date) + " " + CommonUtils.formatDate(item.getDeliveryDate()));

        if (item.getDocStatus().equalsIgnoreCase("02") || item.getDocStatus().equalsIgnoreCase("03") || item.getDocStatus().equalsIgnoreCase("04")) {
            holder.llUpdate.setVisibility(View.VISIBLE);
            holder.llCancel.setVisibility(View.VISIBLE);

            if (item.getDocStatus().equalsIgnoreCase("02")) {
                holder.llUpdate.setVisibility(View.GONE);
            }
        }
        else {
            holder.llCancel.setVisibility(View.GONE);
            holder.llUpdate.setVisibility(View.GONE);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicService.orders = items.get(position);
                Intent intent = new Intent(context, OrderDetail.class);
                context.startActivity(intent);
            }
        });

        holder.llUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestOpenUpdateDialog(item);
            }
        });

        holder.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestCancelOrder(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtIdOrder)
        protected TextView id;
        @BindView(R.id.txtCostOrder)
        protected TextView cost;
        @BindView(R.id.txtDateOrder)
        protected TextView dateorder;
        @BindView(R.id.icon_order_address)
        protected TextView icAddress;
        @BindView(R.id.icon_order_phone)
        protected TextView icphone;
        @BindView(R.id.icon_order_status)
        protected TextView icstatus;
        @BindView(R.id.order_address)
        protected TextView address;
        @BindView(R.id.order_phone)
        protected TextView phone;
        @BindView(R.id.order_status)
        protected TextView status;
        @BindView(R.id.ll_update)
        protected LinearLayout llUpdate;
        @BindView(R.id.txtTypeOrder)
        protected TextView txtType;
        @BindView(R.id.txtDeliveryDate)
        protected TextView txtDeliveryDate;
        @BindView(R.id.txtStatusName)
        protected TextView txtStatusName;
        @BindView(R.id.ll_cancel)
        protected LinearLayout llCancel;


        private View view;

        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            CommonUtils.makeTextViewFont(context, icAddress);
            CommonUtils.makeTextViewFont(context, icphone);
            CommonUtils.makeTextViewFont(context, icstatus);
        }
    }

    private void requestOpenUpdateDialog(final Orders orders) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_order);

        ImageButton close = (ImageButton) dialog.findViewById(R.id.ib_close);
        final Spinner statusSpinner = (Spinner) dialog.findViewById(R.id.spn_status);
        final EditText note = (EditText) dialog.findViewById(R.id.et_note_content);
        chooseDate = (Button) dialog.findViewById(R.id.btn_choose_ship_date);
        Button update = (Button) dialog.findViewById(R.id.btn_update);

        final List<DocStatus> docStatuses = RealmController.getInstance().getDocStatus();
        List<String> docStatusStringList = new ArrayList<>();
        for (DocStatus d : docStatuses) {
            docStatusStringList.add(d.getDocStatusName());
        }

        chooseDate.setOnClickListener(null);
        chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChooseShipDate();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, docStatusStringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(adapter);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String docEntry = orders.getDocEntry();
                String date = chooseDate.getText().toString();
                String status = docStatuses.get(statusSpinner.getSelectedItemPosition()).getDocStatus();
                String snote = note.getText().toString();

                ApiService.getInstance(context).updateDelivery(docEntry, date, status, snote, new ApiService.CommonCallback() {
                    @Override
                    public void onSuccess() {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (containerInstance != null) {
                                    containerInstance.reloadOrderList();
                                }
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onFail(String error) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                String title = context.getString(R.string.app_name);
                                String mess = context.getString(R.string.update_order_fail);
                                CommonUtils.showDialog(context, title, mess);
                                dialog.dismiss();
                            }
                        });
                    }
                });
            }
        });

        dialog.show();
    }

    private void requestCancelOrder(final Orders item) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.app_name)
                .setMessage(R.string.cancel_order_message)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (containerInstance != null) {
                            containerInstance.doCancelOrder(item);
                        }
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void requestChooseShipDate() {
        new DatePickerDialog(context, dataPickListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    Calendar myCalendar = Calendar.getInstance();
    Button chooseDate = null;
    private DatePickerDialog.OnDateSetListener dataPickListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            //this.mStartDateByTM = myCalendar.getTimeInMillis();
            String myFormat = "MM/dd/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

            chooseDate.setText(sdf.format(myCalendar.getTime()));
        }
    };
}
