package com.luan.dms_management.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.luan.dms_management.R;
import com.luan.dms_management.adapter.ProductOfOrderAdapter;
import com.luan.dms_management.adapter.ProductOrderAdapter;
import com.luan.dms_management.models.Customer;
import com.luan.dms_management.models.CustomerGroup;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.ProductCheckForOrder;
import com.luan.dms_management.models.PromotionProduct;
import com.luan.dms_management.models.Route;
import com.luan.dms_management.models.TypeOrder;
import com.luan.dms_management.models.WareHouse;
import com.luan.dms_management.reaml.RealmController;
import com.luan.dms_management.service.ApiService;
import com.luan.dms_management.service.ApiService_ProductAndOrder;
import com.luan.dms_management.service.BasicService;
import com.luan.dms_management.service.SyncService;
import com.luan.dms_management.utils.BaseActivity;
import com.luan.dms_management.utils.CommonUtils;
import com.luan.dms_management.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

import static com.luan.dms_management.utils.Constant.ADD_PROMOTION;

public class CreateOrderDetail extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.orderdetail_cus)
    protected TextView txtName;
    @BindView(R.id.btn_choose_ship_warehouse)
    protected Spinner spiWarehouse;
    @BindView(R.id.btn_choose_ship_date)
    protected Button btnShipDate;
    @BindView(R.id.orderdetail_costpay)
    protected TextView txtCostPay;
    @BindView(R.id.tv_order_create_date)
    protected TextView txtCreateDate;
    @BindView(R.id.spn_choose_type)
    protected Spinner spnType;

    @BindView(R.id.lv_products)
    protected ListView lvProducts;

    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView navigationView;

    Calendar myCalendar = Calendar.getInstance();
    ProductOfOrderAdapter adapter;
    private ArrayList<String> listWareHouse, listWareHouseValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.new_order_cast) + " --");
        ButterKnife.bind(this);

        loadCustomerData();
        configControlEvent();

        BasicService.currentChosenOrderProduct = new ArrayList<>();

        adapter = new ProductOfOrderAdapter(CreateOrderDetail.this, new ArrayList<ProductCheckForOrder>());
        lvProducts.setAdapter(adapter);

        adapter.setOnPriceChanged(new ProductOfOrderAdapter.OnPriceChanged() {
            @Override
            public void onPriceChanged() {
                getSupportActionBar().setTitle(getString(R.string.new_order_cast) + " " + adapter.getCurrentPrice());
                txtCostPay.setText(adapter.getCurrentPrice() + "");
            }
        });
    }

    private void loadWareHouse() {
        listWareHouse = new ArrayList<>();
        listWareHouseValue = new ArrayList<>();
        String first = getString(R.string.choosewarehouse);
        listWareHouse.add(first);
        listWareHouseValue.add("00");
        for (WareHouse wareHouse : RealmController.with(this).getWareHouse()) {
            listWareHouse.add(wareHouse.getWhsName());
            listWareHouseValue.add(wareHouse.getWhsCode());
        }
    }

    private void loadCustomerData() {
        RealmController.with(this).refresh();
        loadWareHouse();
        Customer customer = BasicService.currentCus;

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c = Calendar.getInstance();
        String date = df.format(c.getTime());

        txtName.setText(customer.getCardName());
        txtCreateDate.setText(date);

        initOrderTypeSpinner();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                CreateOrderDetail.this, android.R.layout.simple_spinner_item, listWareHouse);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiWarehouse.setAdapter(adapter);

        showLoading();
        String userCode = ApiService.getInstance(CreateOrderDetail.this).getUserCode();
        String custCode = BasicService.currentCus.getCardCode();
        ApiService.getInstance(CreateOrderDetail.this).GetPromotionProducts(custCode, userCode, date, new ApiService.GetPromotionProductsCallback() {
            @Override
            public void onSuccess(final List<PromotionProduct> data) {
                CreateOrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        BasicService.currentPromotionProduct = data;
                    }
                });
            }

            @Override
            public void onFail(String error) {
                CreateOrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.promotion_get_fail));
                    }
                });
            }
        });
    }

    private void initOrderTypeSpinner() {
        List<TypeOrder> typeOrders = RealmController.with(CreateOrderDetail.this).getTypeOrder();
        List<String> typesData = new ArrayList<>();
        for (TypeOrder t : typeOrders) {
            typesData.add(t.getTypeName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                CreateOrderDetail.this, android.R.layout.simple_spinner_item, typesData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnType.setAdapter(adapter);
        spnType.setSelection(1);
    }

    private String getGroupNameByCode(String code) {
        List<CustomerGroup> groups = RealmController.with(CreateOrderDetail.this).getAllCusGrp();

        for (CustomerGroup g : groups) {
            if (g.getGrpCode().equalsIgnoreCase(code)) {
                return g.getGrpName();
            }
        }

        return "";
    }

    @OnClick(R.id.btn_choose_ship_date)
    protected void onChooseShipDateClick() {
        new DatePickerDialog(CreateOrderDetail.this, CreateOrderDetail.this, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        //this.mStartDateByTM = myCalendar.getTimeInMillis();
        String myFormat = "MM/dd/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        btnShipDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void configControlEvent() {
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_addproduct:
                        onAddProductClick();
                        break;
                    case R.id.action_sale:
                        onSaleClick();
                        break;
                    case R.id.action_save:
                        onSave();
                        break;
                }
                return false;
            }
        });
    }

    private void onAddProductClick() {
        Intent intent = new Intent(CreateOrderDetail.this, AddProductForOrder.class);
        intent.putExtra("custCode", BasicService.currentCus.getCardCode());
        startActivityForResult(intent, Constant.ADD_PRODUCT);
    }

    private void onSaleClick() {
        Intent intent = new Intent(CreateOrderDetail.this, Promotion.class);
        intent.putExtra("custCode", BasicService.currentCus.getCardCode());
        startActivityForResult(intent, ADD_PROMOTION);
    }

    private void onSave() {
        if (btnShipDate.getText().toString().isEmpty()) {
            showErrorDialog(getString(R.string.create_order_missing_delivery_date));
            return;
        }

        if (spiWarehouse.getSelectedItemPosition() == 0) {
            showErrorDialog(getString(R.string.create_order_missing_warehouse));
            return;
        }

        if (spnType.getSelectedItemPosition() == 0) {
            showErrorDialog(getString(R.string.create_order_missing_type));
            return;
        }

        if (adapter.getItems().size() == 0) {
            showErrorDialog(getString(R.string.create_order_missing_product));
            return;
        }

        final AlertDialog dialog = new AlertDialog.Builder(CreateOrderDetail.this)
                .setTitle(R.string.app_name)
                .setMessage(R.string.add_order_product_confirm_save)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        doSave();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void doSave() {
        SyncService syncService = SyncService.getInstance(CreateOrderDetail.this);
        Customer customer = BasicService.currentCus;
        String cardCode = customer.getCardCode();
        String deliveryDate = btnShipDate.getText().toString();
        String cardName = customer.getCardName();
        String userCode = ApiService.getInstance(CreateOrderDetail.this).getUserCode();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String orderDate = df.format(c.getTime());
        String whsCode = RealmController.with(this).getWareHouse().get(spiWarehouse.getSelectedItemPosition() - 1).getWhsCode();

        List<ApiService_ProductAndOrder.ProductToAdd> productToAdds = new ArrayList<>();
        for (ProductCheckForOrder p : adapter.getItems()) {
            String itemCode = p.getItem().getItemCode();
            String itemName = p.getItem().getItemName();
            String itemType = isPromotionProduct(p) ? "1" : "0";
            String quantity = p.getQuantity() == null ? "0" : p.getQuantity();
            String uom = p.getItem().getBuyUoM();
            String price = p.getPrice();
            String vat = p.getVAT();

            int intQuantity = Integer.parseInt(quantity);
            double intPrice = Double.parseDouble(price);
            double intVat = Double.parseDouble(vat);

            ApiService_ProductAndOrder.ProductToAdd productToAdd = new ApiService_ProductAndOrder.ProductToAdd();
            productToAdd.itemCode = itemCode;
            productToAdd.itemName = itemName;
            productToAdd.quantity = quantity;
            productToAdd.uoM = uom;
            productToAdd.price = price;
            productToAdd.discount = "0";
            productToAdd.priceAfterDiscount = price;
            productToAdd.lineTotal = String.valueOf(intPrice * intQuantity);
            productToAdd.vatPercent = vat;
            productToAdd.vatAmt = String.valueOf((intQuantity * intPrice) * (intVat / 100));
            productToAdd.gTotal = String.valueOf((intPrice * (intVat + 100) * intQuantity) / 100);
            productToAdd.itemType = itemType;
            productToAdd.whsCode = whsCode;

            productToAdds.add(productToAdd);
        }

        ApiService_ProductAndOrder.OrderToCreate orderToCreate = new ApiService_ProductAndOrder.OrderToCreate();
        orderToCreate.cardCode = cardCode;
        orderToCreate.orderType = RealmController.with(CreateOrderDetail.this).getTypeOrder().get(spnType.getSelectedItemPosition()).getTypeCode();
        orderToCreate.poDate = orderDate;
        orderToCreate.deliveryDate = deliveryDate;
        orderToCreate.salesEmp = ApiService.getInstance(CreateOrderDetail.this).getSaleManID();
        orderToCreate.docTotal = getDocTotal(productToAdds);
        orderToCreate.GTotal = getGTotal(productToAdds);
        orderToCreate.vatTotal = getVATTotal(productToAdds);
        orderToCreate.remarks = "";

        syncService.offlineAddOrderProduct(orderToCreate, productToAdds);

        showLoading();
        syncService.sync(new ApiService.CommonCallback() {
            @Override
            public void onSuccess() {
                CreateOrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        CreateOrderDetail.this.finish();
                    }
                });
            }

            @Override
            public void onFail(String error) {
                CreateOrderDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        CreateOrderDetail.this.finish();
                    }
                });
            }
        });
    }

    private String getDocTotal(List<ApiService_ProductAndOrder.ProductToAdd> data) {
        double price = 0;
        for (ApiService_ProductAndOrder.ProductToAdd p : data) {
            price += Double.parseDouble(p.lineTotal);
        }

        return String.valueOf(price);
    }

    public String getGTotal(List<ApiService_ProductAndOrder.ProductToAdd> data) {
        double price = 0;

        for (ApiService_ProductAndOrder.ProductToAdd p : data) {
            price += Double.parseDouble(p.gTotal);
        }

        return String.valueOf(price);
    }

    public String getVATTotal(List<ApiService_ProductAndOrder.ProductToAdd> data) {
        double price = Double.parseDouble(getGTotal(data)) - Double.parseDouble(getDocTotal(data));

        return String.valueOf(price);
    }

    private boolean isPromotionProduct(ProductCheckForOrder data) {
        List<PromotionProduct> rootData = BasicService.currentPromotionProduct;
        for (PromotionProduct p : rootData) {
            if (p.getItemCode().equalsIgnoreCase(data.getItem().getItemCode())) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.ADD_PRODUCT && resultCode == RESULT_OK) {
            adapter.updateData(BasicService.justAddedProductForOrder);

            getSupportActionBar().setTitle(getString(R.string.new_order_cast) + " " + adapter.getCurrentPrice());
            txtCostPay.setText(adapter.getCurrentPrice() + "");

            CommonUtils.setListViewHeightBasedOnChildrenCart(CreateOrderDetail.this, lvProducts);
        }

        if (requestCode == Constant.ADD_PROMOTION && resultCode == RESULT_OK) {
            adapter.updateData(BasicService.currentChosenOrderProduct);

            getSupportActionBar().setTitle(getString(R.string.new_order_cast) + " " + adapter.getCurrentPrice());
            txtCostPay.setText(adapter.getCurrentPrice() + "");

            CommonUtils.setListViewHeightBasedOnChildrenCart(CreateOrderDetail.this, lvProducts);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            CommonUtils.makeToast(this, "ok");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
