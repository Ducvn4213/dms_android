package com.luan.dms_management.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by luann on 30/10/2017.
 */

public class ItemType extends RealmObject {
    @PrimaryKey
    private String ItemType;
    private String OrderTypeName;

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String typeCode) {
        ItemType = typeCode;
    }

    public String getOrderTypeName() {
        return OrderTypeName;
    }

    public void setOrderTypeName(String typeName) {
        OrderTypeName = typeName;
    }
}
