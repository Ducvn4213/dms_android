package com.luan.dms_management.service;

import android.text.TextUtils;

import com.luan.dms_management.models.LeaveLog;
import com.luan.dms_management.models.LeaveReason;
import com.luan.dms_management.models.LeaveType;
import com.luan.dms_management.models.TimeSheet;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class ApiService_Customer extends ApiService_Work {
    public void AddNewCustomer(String cardName, String contactPerson, String tel, String lat, String lon, String group, String remark, String address, final AddCustCallback callback) {
        String link = String.format(getURL() + Config.ADDCUST);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CardName", cardName);
        params.add(param);
        param = new Param("ContactPerson", contactPerson);
        params.add(param);
        param = new Param("Tel", tel);
        params.add(param);
        param = new Param("LatitudeValue", lat);
        params.add(param);
        param = new Param("LongitudeValue", lon);
        params.add(param);
        param = new Param("CardGroup", group);
        params.add(param);
        param = new Param("Reamarks", remark);
        params.add(param);
        param = new Param("Address", address);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultString")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                String data = xpp.getText();
                                callback.onSuccess(data);
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void EditCustomer(String cardCode, String cardName, String contactPerson, String tel, String lat, String lon, String group, String remark, String address, final CommonCallback callback) {
        String link = String.format(getURL() + Config.EDITCUST);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CardName", cardName);
        params.add(param);
        param = new Param("CardCode", cardCode);
        params.add(param);
        param = new Param("ContactPerson", contactPerson);
        params.add(param);
        param = new Param("Tel", tel);
        params.add(param);
        param = new Param("LatitudeValue", lat);
        params.add(param);
        param = new Param("LongitudeValue", lon);
        params.add(param);
        param = new Param("UserCode", getUserCode());
        params.add(param);
        param = new Param("Address", address);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultString")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                String data = xpp.getText();
                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

}
