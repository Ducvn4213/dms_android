package com.luan.dms_management.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.luan.dms_management.utils.PrefUtils;
import com.luan.dms_management.utils.TrackerGPS;

import static com.luan.dms_management.utils.Constant.NO_SEC_REFREST;

public class UpdateLocationService extends IntentService {

    ApiService apiService = ApiService.getInstance(getBaseContext());

    String currentBatLevel = "";
    int retryCount = 0;

    private final Handler handler = new Handler();
    private String SEQUENCE_TIME = "1";

    public UpdateLocationService() {
        super("UpdateLocationService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        SEQUENCE_TIME = PrefUtils.getPreference(getBaseContext(), NO_SEC_REFREST);

        int theTime = Integer.parseInt(SEQUENCE_TIME) * 1000;
        handler.postDelayed(workRunnable, theTime);
        getCurrentLocation();
    }

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            currentBatLevel = String.valueOf(level);
        }
    };

    private void getCurrentLocation() {
        if (retryCount > 5) {
            afterGotCurrentLocation(0, 0);
            return;
        }
        retryCount++;

        TrackerGPS mTrackerGPS = new TrackerGPS(getBaseContext());
        if (mTrackerGPS.canGetLocation()) {

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                getCurrentLocation();
                return;
            }

            afterGotCurrentLocation(latitude, longitude);
        } else {
            mTrackerGPS.showSettingsAlert();
        }
    }

    private void afterGotCurrentLocation(double lat, double lon) {
        String userCode = apiService.getUserCode();
        String deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        apiService.updateLocation(userCode, String.valueOf(lat), String.valueOf(lon), currentBatLevel, deviceID);

        int theTime = Integer.parseInt(SEQUENCE_TIME) * 1000;
        handler.postDelayed(workRunnable, theTime);
    }

    Runnable workRunnable = new Runnable() {
        @Override
        public void run() {
            getCurrentLocation();
        }
    };
}
