package com.luan.dms_management.utils;

/**
 * Created by luan.nt on 7/31/2017.
 */

public class Constant {
    public static int PICK_IMAGE_REQUEST = 1;
    public static int ADD_PRODUCT = 0000;
    public static int ADD_PROMOTION = 1111;
    public static int PERMISSIONS = 0000;
    public static final String USERNAME_PREF_KEY = "USERNAME_PREF_KEY";
    public static final String PASSWORD_PREF_KEY = "PASSWORD_PREF_KEY";
    public static final String LANGUAGE_PREF_KEY = "LANGUAGE_PREF_KEY";
    public static final String WORKING_PREF_KEY = "WORKING_PREF_KEY";
    public static final String NO_SEC_REFREST = "NO_SEC_REFREST";
    public static final String SALEID_PREF_KEY = "SALEID_PREF_KEY";
    public static final String LAT_PREF_KEY = "LAT_PREF_KEY";
    public static final String LONG_PREF_KEY = "LONG_PREF_KEY";

    public static final String PRODUCT_PREF_KEY = "PRODUCT_PREF_KEY";
    public static final String STAFF_PREF_KEY = "STAFF_PREF_KEY";
    public static final String CUSTOMER_PREF_KEY = "CUSTOMER_PREF_KEY";
    public static final String TIMEID_PREF_KEY = "TIMEID_PREF_KEY";
    public static final String STARTEND_PREF_KEY = "STARTEND_PREF_KEY";
}
