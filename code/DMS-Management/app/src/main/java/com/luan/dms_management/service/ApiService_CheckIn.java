package com.luan.dms_management.service;

import android.text.TextUtils;

import com.luan.dms_management.models.Channel;
import com.luan.dms_management.models.Customer;
import com.luan.dms_management.models.CustomerGroup;
import com.luan.dms_management.models.NormalProduct;
import com.luan.dms_management.models.NoteType;
import com.luan.dms_management.models.OrderbyDoc;
import com.luan.dms_management.models.Orders;
import com.luan.dms_management.models.Product;
import com.luan.dms_management.models.Route;
import com.luan.dms_management.models.Staff;
import com.luan.dms_management.models.StaffLocation;
import com.luan.dms_management.models.StaffMonitor;
import com.luan.dms_management.models.StatusOrder;
import com.luan.dms_management.models.TypeOrder;
import com.luan.dms_management.models.TypeVisit;
import com.luan.dms_management.models.WareHouse;
import com.luan.dms_management.utils.CommonUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class ApiService_CheckIn extends ApiService_LoadData{
    public void checkin(String customerCode, String userCode, String deviceID, final CheckinCallback callback) {
        String link = String.format(getURL() + Config.CHECKIN);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("DeviceID", deviceID);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                String data = xpp.getText();
                                callback.onSuccess(data);
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void checkout(String customerCode, String userCode, String checkInID, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CHECKOUT);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("CheckInID", checkInID);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void checkinAddLog(String customerCode, String userCode, String checkInID, String notesGroup, String remark, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CHECKINNOTE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CustCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("CheckInID", checkInID);
        params.add(param);
        param = new Param("NotesGroup", notesGroup);
        params.add(param);
        param = new Param("NotesRemark", remark);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void checkInAddPicture(String checkInID, String customerCode, String userCode, String pic1, String pic2, String pic3, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CHECKINADDIMAGE);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CheckInID", checkInID);
        params.add(param);
        param = new Param("CustCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("Pics1", pic1);
        params.add(param);
        param = new Param("Pics2", pic2);
        params.add(param);
        param = new Param("Pics3", pic3);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void checkinInstock(String customerCode, String userCode, String checkInID, String itemCode, String barCode, String caseInStock, String bottleInStock, String remark, final CommonCallback callback) {
        String link = String.format(getURL() + Config.CHECKINSTOCK);

        List<Param> params = new ArrayList<>();
        Param param = new Param("CheckInID", checkInID);
        params.add(param);
        param = new Param("CustCode", customerCode);
        params.add(param);
        param = new Param("UserCode", userCode);
        params.add(param);
        param = new Param("ItemCode", itemCode);
        params.add(param);
        param = new Param("BarCode", barCode);
        params.add(param);
        param = new Param("CaseInStock", caseInStock);
        params.add(param);
        param = new Param("BottleInStock", bottleInStock);
        params.add(param);
        param = new Param("Remarks", remark);
        params.add(param);

        mNetwork.executePost(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                XmlPullParserFactory factory = null;
                try {
                    boolean readyToGetResult = false;

                    factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();

                    xpp.setInput(new StringReader(response));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_DOCUMENT) {
                            //TODO: do nothing
                        } else if (eventType == XmlPullParser.START_TAG) {
                            if (xpp.getName().equalsIgnoreCase("ResultID")) {
                                readyToGetResult = true;
                            }
                        } else if (eventType == XmlPullParser.TEXT) {
                            if (readyToGetResult) {
                                callback.onSuccess();
                                return;
                            }
                        }
                        eventType = xpp.next();
                    }

                    callback.onFail("");

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

}
